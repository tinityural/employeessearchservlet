import com.ibm.commons.runtime.Application;
import com.ibm.commons.runtime.Context;
import com.ibm.commons.runtime.RuntimeFactory;
import com.ibm.commons.runtime.impl.app.RuntimeFactoryStandalone;
import com.ibm.commons.util.io.json.JsonJavaArray;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.sbt.security.authentication.AuthenticationException;
import com.ibm.sbt.services.client.connections.profiles.ProfileService;
import com.ibm.sbt.services.endpoints.BasicEndpoint;
import com.ibm.sbt.services.endpoints.ConnectionsBasicEndpoint;
import com.trinity.profiles.FieldNames;
import com.trinity.profiles.Profile;
import com.trinity.profiles.ProfileCollection;
import com.trinity.profiles.ProfileEntry;
import com.trinity.profiles.search.LookupDataSource;
import com.trinity.profiles.search.impl.ConnLookupDataSource;
import com.trinity.profiles.search.impl.DominoLookupDataSource;
import com.trinity.utils.DominoSettingsFields;
import com.trinity.utils.ProfileUtils;
import lotus.domino.*;
import org.apache.http.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;


public class ProfilesSearch extends HttpServlet {
    private Application application;
    private Context context;
    private String dbName;
    private static final String DB_NAME_PARAM = "db_name";


    private Map<String, String> parseQuery(String query) throws IllegalAccessException, ParseException, UnsupportedEncodingException {
        String[] paramsStrings = query.split("&");
        Map<String, String> retMap = new HashMap<>();

        for (String paramStr : paramsStrings) {
            String[] kvArr = paramStr.split("=", 2);

            if (kvArr.length != 2) {
                throw new ParseException("Failed to parse GET query.");
            }

            retMap.put(kvArr[0], URLDecoder.decode(kvArr[1], "UTF-8"));
        }

        for (String paramKey : retMap.keySet()) {
            if (!FieldNames.valuesSet().contains(paramKey) && !paramKey.equals("search")) {
                throw new ParseException("Failed to parse GET query. Unknown fieldValue " + paramKey + ".");
            }
            if(paramKey.equals(FieldNames.PHONE_NUMBER)) {
                String val = retMap.get(paramKey);

                retMap.put(FieldNames.PHONE_NUMBER, val);
                retMap.put(FieldNames.INTERNAL_PHONE, val);
                retMap.put(FieldNames.EXTERNAL_PHONE, val);
            }
            else if(paramKey.equals(FieldNames.FIRST_NAME)) {
                String val = retMap.get(paramKey);

                retMap.put(FieldNames.FIRST_NAME, val);
                retMap.put(FieldNames.LAST_NAME, val);
                retMap.put(FieldNames.MIDDLE_NAME, val);
            }
        }

        return retMap;
    }

    private BasicEndpoint getConnEndpoint(Document settings) throws NotesException, AuthenticationException {
        BasicEndpoint ep = new ConnectionsBasicEndpoint();
        ep.setUrl(settings.getItemValueString(DominoSettingsFields.CONN_URL_FIELD.toString()));
        ep.setForceTrustSSLCertificate(true);
        ep.login(
                settings.getItemValueString(DominoSettingsFields.CONN_USER_FIELD.toString()),
                settings.getItemValueString(DominoSettingsFields.CONN_PASSWORD_FIELD.toString())
        );

        return ep;
    }

    @Override
    public void init() throws ServletException {
        super.init();

        RuntimeFactory runtimeFactory = new RuntimeFactoryStandalone();
        application = runtimeFactory.initApplication(null);
        context = Context.init(this.application, null, null);
    }

    @Override
    public void destroy() {
        super.destroy();
        Context.destroy(this.context);
        Application.destroy(this.application);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NotesThread.sinitThread();

        if(application == null) {
            RuntimeFactory runtimeFactory = new RuntimeFactoryStandalone();
            application = runtimeFactory.initApplication(null);
        }

        if(context == null) {
            context = Context.init(application, null, null);
        }

        if(dbName == null || dbName.equals("")) {
            dbName = getInitParameter(DB_NAME_PARAM);
        }

        JsonJavaObject respJson = new JsonJavaObject();
        JsonJavaArray employees = new JsonJavaArray();

        PrintWriter pw = resp.getWriter();

        resp.setContentType("application/json; charset=utf-8");

        try {
            Map<String, String> lookupParams = parseQuery(req.getQueryString());

            Session session = NotesFactory.createSession();
            Database db = session.getDatabase("", dbName);
            Document settings = db.getView("Settings").getFirstDocument();

            ProfileService profileSvc = new ProfileService(getConnEndpoint(settings));

            LookupDataSource dmnLookupSource = new DominoLookupDataSource(db);
            ProfileCollection dmnProfiles = dmnLookupSource.lookup(lookupParams);

            for(Profile dmnProfile : dmnProfiles) {
                String uid = dmnProfile.getUid();

                if(uid != null & !Objects.equals(uid, "")) {
                    com.ibm.sbt.services.client.connections.profiles.Profile lcProfile = profileSvc.getProfile(uid);
                    if(lcProfile != null && lcProfile.getEmail() != null) {
                        Profile foundProfile = ProfileUtils.getProfile(profileSvc.getProfile(uid));

                        for(ProfileEntry<?> dmnEntry : dmnProfile.entries()) {
                            if(dmnEntry != null && dmnEntry.getValue() != null) {
                                foundProfile.putEntry(dmnEntry);
                            }
                        }

                        for(ProfileEntry lcEntry : ConnLookupDataSource.getProfileEntries(lcProfile)) {
                            String fieldName = lcEntry.getName();
                            Object value = lcEntry.getValue();
                            if(!dmnProfiles.fields().contains(fieldName) && value != null) {
                                foundProfile.putEntry(fieldName, value);
                            }
                        }

                        foundProfile.putEntry(FieldNames.SERVICES, ProfileUtils.getProfileServices(settings, foundProfile));
                        foundProfile.putEntry(FieldNames.PROFILE_PAGE, ProfileUtils.getProfilePage(settings, foundProfile));

                        employees.add(ProfileUtils.profileToJson(foundProfile));
                    }
                }
            }



            respJson.put("result", "success");
            respJson.put("employees", employees);
        } catch (IllegalAccessException | ParseException | AuthenticationException | NotesException e) {
            respJson.put("result", "failed");
            respJson.put("statusMessage", e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            respJson.put("result", "failed");
            respJson.put("statusMessage", "Internal error. " + e.getMessage());
            e.printStackTrace();
        } finally {
            pw.println(respJson);
            NotesThread.stermThread();
        }
    }
}
